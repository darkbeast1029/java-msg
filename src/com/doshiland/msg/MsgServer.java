package com.doshiland.msg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MsgServer {
  public static void main(String[] args) throws IOException {
    ServerSocket s = new ServerSocket(1234);
    System.out.println("Listening on port 1234");
    Scanner scan = new Scanner(System.in);
    while (true) {
      Socket c = s.accept();
      System.out.println("Connection accepted: " + c.getInetAddress());
      PrintWriter writer = new PrintWriter(c.getOutputStream());
      writer.println("Lets chat.");
      writer.flush();
      BufferedReader buff = new BufferedReader(new InputStreamReader(
          c.getInputStream()));
      for(String line = buff.readLine(); line != null; line = buff.readLine()) {
        System.out.println(line);
        writer.println(scan.nextLine());
        writer.flush();
      }
      s.close();
      scan.close();
      writer.close();
      c.close();
    }
  }
}
