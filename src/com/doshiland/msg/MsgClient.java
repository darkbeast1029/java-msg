package com.doshiland.msg;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MsgClient {
  public static void main(String[] args) throws UnknownHostException, IOException {
    Socket c = new Socket("localhost", 1234);
    BufferedReader buff = new BufferedReader(new InputStreamReader(
        c.getInputStream()));
    PrintWriter writer = new PrintWriter(c.getOutputStream());
    Scanner scan = new Scanner(System.in);
    for(String line = buff.readLine(); line != null; line = buff.readLine()) {
      System.out.println(line);
      writer.println(scan.nextLine());
      writer.flush();
    }

    scan.close();
    writer.close();
    c.close();
  }
}
